import json
import os
from typing import Optional

import gitlab
from fastapi import FastAPI, Request
from pydantic import BaseModel

OBJECT_ATTRIBUTES = "object_attributes"
ID = "id"
IID = "iid"
PROJECT = "project"
ACTION = "action"
CLOSE = "close"
ISSUE = "issue"
LABELS = "labels"
TITLE = "title"
UNLOCKABLE = "unlockable"


class CoursewareBot:
    def __init__(self):
        self._api_token = os.environ["API_TOKEN"]
        self.gl = gitlab.Gitlab("https://gitlab.com", private_token=self._api_token)

    def unlock_issue(self, issue):
        print("Unlocking issue")

    def switch_badges(self, issue_iid, project_id):
        project = self.gl.projects.get(project_id)
        badges = project.badges.list()
        for badge in badges:
            if f"/issues/{issue_iid}" in badge.link_url:
                print("Found old badge")
                issue_url = badge.link_url
                badge.delete()
                print("Creating a new badge awarding completion")
                image_url = f"https://img.shields.io/badge/issue{issue_iid}-done!-green"
                project.badges.create({"image_url": image_url, "link_url": issue_url})


app = FastAPI()
bot = CoursewareBot()


@app.post("/issue")
async def check_issue(request: Request):
    body = await request.json()
    if body[OBJECT_ATTRIBUTES][ACTION] != CLOSE:
        return 200
    issue_iid = body[OBJECT_ATTRIBUTES][IID]
    print(issue_iid)
    issue_labels = body[OBJECT_ATTRIBUTES][LABELS]
    print(issue_labels)
    project_id = body[PROJECT][ID]

    bot.switch_badges(issue_iid, project_id)
