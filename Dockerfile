FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7

WORKDIR /app
COPY ./app /app
COPY . /app

RUN pip3 install poetry
RUN poetry export -f requirements.txt --output requirements.txt --without-hashes

RUN pip3 install -r requirements.txt